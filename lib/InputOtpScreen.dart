import 'dart:async';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'HomePage.dart';
import 'InputPhoneNumberScreen.dart';
import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:flutter_otp/flutter_otp.dart';

class InputOtpScreen extends StatefulWidget {
  // this is the helper object to bring the phoneNumber and otp from the InputPhoneNumberScreen to InputOtpScreen
  TakeGeneratedOtpToOtpScreen bringOtpFromPhoneScreen;

  InputOtpScreen({Key key, this.bringOtpFromPhoneScreen}) : super(key: key);

  @override
  _InputOtpScreenState createState() => _InputOtpScreenState();
}

class _InputOtpScreenState extends State<InputOtpScreen> {
  // for checking if resend button is clickable or not
  bool isResendButtonEnabled = false;
  bool _isTimeOut = false;
  bool isOtpResend = false;

  //Create an private object of FlutterOtp to send otp to the device
  FlutterOtp _otpHelperObject = new FlutterOtp();

  // otp should be 4 digit long
  int _minOtp = 1000;
  int _maxOtp = 9999;

  String _phoneNumber;
  int _otpEntered;
  int _otpGenerated;
  int _otpGeneratedResend = 0000;

  // timeout duration
  int duration = 10;
  //int durationForShowingTimer = 10;

  // this function enables the resend otp button
  void enableButton() {
    setState(() {
      isResendButtonEnabled = true;
    });
  }

  // this function disables the resend otp button
  void disableButton() {
    setState(() {
      isResendButtonEnabled = false;
    });
  }

  void resendOtpFunction() {
    //print("clicked");
    //print(widget.bringOtpFromPhoneScreen.phoneNumber);
    _phoneNumber = widget.bringOtpFromPhoneScreen.phoneNumber;
    _otpGeneratedResend = _getRandomOTP();

    //print("This is generated otp :"+_otpGeneratedResend.toString());

    String _countryCode = "+91";

    // send the generated otp after resend button pressed
    _otpHelperObject.sendOtp(
        _phoneNumber,
        "Your OTP is : " + _otpGeneratedResend.toString(),
        _minOtp,
        _maxOtp,
        _countryCode);
  }

  // function for generating random otp
  int _getRandomOTP() {
    int generatedOtp = _minOtp + Random().nextInt(_maxOtp - _minOtp);
    return generatedOtp;
  }

  // function for validating the otp
  bool _checkOTP(int otpGenerated, int otpEntered) {
    if (otpGenerated == otpEntered) {
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: duration), () {
      _isTimeOut = true;

      // for checking if the time is out
      if (_isTimeOut) {
        _isTimeOut = false;
      } else {
        _isTimeOut = true;
      }

      // for enableing and disabling resend button
      if (!isResendButtonEnabled) {
        // disableButton();
        enableButton();
      }
      // else {
      //   enableButton();
      // }
    });

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.yellow[100],
      body: Container(
        padding: EdgeInsets.all(25.0),
        child: Column(
          children: [
            SizedBox(
              height: 50.0,
            ),

            Text(
              "Otp sent to +91 ${widget.bringOtpFromPhoneScreen.phoneNumber} mobile number",
              style: TextStyle(
                color: Colors.blue,
                fontSize: 15.0,
              ),
            ),
            SizedBox(
              height: 30.0,
            ),

            // for showing the animated countDownTimer
            CircularCountDownTimer(
                width: 80,
                height: 80,
                duration: duration,
                fillColor: Colors.blue,
                color: Colors.white,
                isReverse: true),

            SizedBox(
              height: 50.0,
            ),

            // otp entering field
            TextFormField(
              decoration: InputDecoration(
                labelText: "Enter the OTP here",
                border: OutlineInputBorder(),
              ),
              onChanged: (enteredOtp) {
                _otpEntered = int.parse(enteredOtp);
              },
            ),

            SizedBox(
              height: 30.0,
            ),

            // resend button
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                CupertinoButton.filled(
                  onPressed:
                      isResendButtonEnabled ? () => resendOtpFunction() : null,
                  //   if (isResendButtonEnabled) {
                  //     isOtpResend = true;
                  //     return resendOtpFunction();
                  //   }
                  //   return null;
                  // },
                  //color: Colors.pinkAccent,
                  //textColor: Colors.white,
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: Text("Resend OTP"),
                ),
              ],
            ),

            SizedBox(
              height: 30.0,
            ),

            // submit button
            CupertinoButton.filled(
              child: Text("Submit"),
              onPressed: () {
                // this is the otp came from InputPhoneNumberScreen
                _otpGenerated = int.parse(widget
                    .bringOtpFromPhoneScreen.otpFromPhoneScreenToOtpScreen);

                // check if entered otp matched with the generated otp from resend button or the otp from the InputPhoneNumberScreen
                bool isCorrectOtp;

                if (isResendButtonEnabled) {
                  print("$isOtpResend in if ");
                  isCorrectOtp = (_checkOTP(_otpGeneratedResend, _otpEntered));
                } else {
                  print("$isOtpResend in else");
                  isCorrectOtp = (_checkOTP(_otpGenerated, _otpEntered));
                }
                //print(isCorrectOtp);

                //if the entered otp is correct and the time is not out  go to Home
                if (isCorrectOtp && !_isTimeOut) {
                  // Navigator.pushReplacement(context,
                  //     MaterialPageRoute(builder: (BuildContext context) => HomePage()));
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => HomePage()),
                      (route) => false);
                  //Navigator.of(context).pushNamed('/HomePage');
                } else {
                  //show snackbar and stay in the current screen
                  print("Please enter the correct OTP or time out ");
                  //Navigator.of(context).pushNamed('/InputPhoneNumberScreen');
                }
              },
              //color: Colors.green[400],
            ),

            SizedBox(
              height: 30.0,
            ),

            // Row(
            //   mainAxisAlignment: MainAxisAlignment.end,
            //   children: <Widget>[
            //
            //     // go to InputPhoneNumberScreen from this screen
            //     FlatButton(onPressed: (){
            //       Navigator.of(context).pushNamed('/InputPhoneNumberScreen');
            //     },
            //         child: Text("Enter the phone number again",
            //         style: TextStyle(
            //           color: Colors.blue
            //         ),
            //         ),
            //     ),
            //
            //   ],
            // )
          ],
        ),
      ),
    );
  }
}
