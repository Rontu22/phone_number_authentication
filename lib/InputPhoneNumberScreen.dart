import 'dart:math';

import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_otp/flutter_otp.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:phone_auth_otp/HomePage.dart';
import 'package:phone_auth_otp/InputOtpScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sms/sms.dart';

// class for transferring the data from the InputPhoneNumberScreen to InputOtpScreen
class TakeGeneratedOtpToOtpScreen {
  String otpFromPhoneScreenToOtpScreen;
  String phoneNumber;

  TakeGeneratedOtpToOtpScreen(
      this.otpFromPhoneScreenToOtpScreen, this.phoneNumber);
}

class InputPhoneNumberScreen extends StatefulWidget {
  @override
  _InputPhoneNumberScreenState createState() => _InputPhoneNumberScreenState();
}

class _InputPhoneNumberScreenState extends State<InputPhoneNumberScreen> {
  String _phoneNumber;
  String _countryCode = "+91";
  int _otpGenerated;

  // otp should be 4 digit long
  int _minOtp = 1000;
  int _maxOtp = 9999;

  bool validatePhoneNumber(String phoneNumber) {
    String pattern = r'(^(?:[+0]9)?[0-9]{10}$)';
    RegExp regExp = new RegExp(pattern);

    if (phoneNumber.length == 0) {
      return false;
    } else if (!regExp.hasMatch(phoneNumber)) {
      return false;
    }
    return true;
  }

  // this is going to be the auto login screen
  bool isLoggedIn = false;
  String STORED_PHONE_NUMBER = "STORED_PHONE_NUMBER";
  final storage = new FlutterSecureStorage();
  @override
  void initState() {
    super.initState();

    //print("\n\n\n\n\n\n\n Phone Number = $isLoggedIn\n\n\n\n\n\n\n");
    autoLoggedIn();
    //autoLogIn();
  }

  void autoLoggedIn() async {
    String phoneNumberInLocalStorage =
        await storage.read(key: STORED_PHONE_NUMBER);
    //print("\n\n\n\n\n\n\n Phone Number = $phone\n\n\n\n\n\n\n");

    if (phoneNumberInLocalStorage == null) {
      isLoggedIn = false;
    } else {
      isLoggedIn = true;
    }
  }

  //Create an private object of FlutterOtp to send otp to the device
  FlutterOtp _otpHelperObject = new FlutterOtp();
  @override
  Widget build(BuildContext context) {
    if (isLoggedIn) {
      return Scaffold(
        body: HomePage(),
      );
    }
    return Scaffold(
      // for the keyboard error : bottom overflowed by pixels
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.yellow[100],

      body: Center(
        child: Container(
          padding: EdgeInsets.all(25.0),
          child: Column(
            children: [
              SizedBox(
                height: 100.0,
              ),

              // placing the logo
              CircleAvatar(
                backgroundImage: NetworkImage(
                    "https://cdn.pixabay.com/photo/2019/11/19/11/47/tealight-4637226_1280.jpg"),
                radius: 70,
              ),

              SizedBox(
                height: 40.0,
              ),

              // Phone Number entering field
              TextFormField(
                decoration: InputDecoration(
                    labelText: "Enter Your Phone Number",
                    border: OutlineInputBorder(),
                    suffixIcon: Icon(
                      Icons.phone,
                    )),
                validator: (value) {
                  if (value.isEmpty) {
                    return "Please enter some text";
                  }
                },
                onChanged: (phoneNumberEntered) {
                  setState(() {
                    // get the phone number from the user
                    _phoneNumber = phoneNumberEntered;
                    //print(_phoneNumber);
                  });
                },
              ),

              SizedBox(
                height: 40.0,
              ),

              CupertinoButton.filled(
                child: new Text("Send OTP"),
                onPressed: () {
                  //store the phone number locally
                  storage.write(key: STORED_PHONE_NUMBER, value: _phoneNumber);

                  if (validatePhoneNumber(_phoneNumber)) {
                    // print(_phoneNumber);
                    // generate a random OTP and take the value
                    _otpGenerated = _getRandomOTP();

                    // here we are sending the _otpGenerated to InputOtpScreen for validation
                    _navigateToOtpScreen(
                        context, _otpGenerated.toString(), _phoneNumber);

                    // here we are sending the otp to the device
                    _otpHelperObject.sendOtp(
                        _phoneNumber,
                        "Your OTP is : " + _otpGenerated.toString(),
                        _minOtp,
                        _maxOtp,
                        _countryCode);
                  } else {
                    Scaffold.of(context).showSnackBar(SnackBar(
                        content: Text('Please enter the correct otp...')));
                  }
                },

                // set the colors and padding and all other properties according to the app design
                //: Colors.blue[200],
              ),
            ],
          ),
        ),
      ),
    );
  }

  // function for generating random otp
  int _getRandomOTP() {
    int generatedOtp = _minOtp + Random().nextInt(_maxOtp - _minOtp);
    return generatedOtp;
  }

  // function for transferring the data from InputPhoneNumberScreen to InputOtpScreen
  void _navigateToOtpScreen(
      BuildContext context, String otpGenerated, String phoneNumber) async {
    // takeOtpToOtpScreenForVerification <= is a helper object
    TakeGeneratedOtpToOtpScreen takeOtpToOtpSceenForVerification =
        new TakeGeneratedOtpToOtpScreen(otpGenerated, phoneNumber);

    // going to the next screen and carrying the data through the helper class we defined above
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => InputOtpScreen(
                  bringOtpFromPhoneScreen: takeOtpToOtpSceenForVerification,
                )));
    //print(result);
  }
}
