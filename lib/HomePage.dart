import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: Center(
        child: Text(
          "You Logged in ! Welcome to home ...",
          style: TextStyle(
            fontFamily: "cursive",
            fontSize: 20.0,
          ),
        ),
      ),
    );
  }
}
